# qutebrowser

These are my qutebrowser dotfiles.

## Location

These dotfiles should exist under the `~/.config/qutebrowser` directory in Linux, and `~/.qutebrowser` on macOS.
